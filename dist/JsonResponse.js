"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var class_transformer_1 = require("class-transformer");
/**
 * @package common
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class JsonResponse
 */
var JsonResponse = /** @class */ (function () {
    function JsonResponse(data, status, message) {
        if (status === void 0) { status = true; }
        if (message === void 0) { message = "result"; }
        this.status = true;
        this.message = "result";
        this.data = data;
        this.status = status;
        this.message = message;
    }
    __decorate([
        class_transformer_1.Expose(),
        __metadata("design:type", Object)
    ], JsonResponse.prototype, "data", void 0);
    __decorate([
        class_transformer_1.Expose(),
        __metadata("design:type", Boolean)
    ], JsonResponse.prototype, "status", void 0);
    __decorate([
        class_transformer_1.Expose(),
        __metadata("design:type", String)
    ], JsonResponse.prototype, "message", void 0);
    return JsonResponse;
}());
exports.JsonResponse = JsonResponse;
//# sourceMappingURL=JsonResponse.js.map