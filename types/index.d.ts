/**
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @copyright
 * @description
 */
import {ArgumentsHost, ExceptionFilter} from "@nestjs/common";

export declare class JsonResponse<T = any> {
    constructor(data: T);
    constructor(data: T, status: boolean);
    constructor(data: T, status: boolean, message: string)
}

export declare class SocketResponse<T = any> {
    constructor(event: string, data: T);
}

export declare class HttpExceptionFilter implements ExceptionFilter{
    catch(exception: any, host: ArgumentsHost): any;
}

export declare class WsExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost): any;
}

export * from "./../src/PaginationInterface";
