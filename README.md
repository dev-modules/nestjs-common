## Standard class for simplify working with *NestJS* framework

### JsonResponse
```typescript
@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class HelloController {
    @Get('hello')
    public async hello(): Promise<JsonResponse<string>> {
        return new JsonResponse<string>(
            "Hello world",
            true,
            "GET /hello"
        );
    }
}
```

Result will be
```json
{
  "status": true,
  "message": "GET /hello",
  "data": "Hello world"
}
```

* Data can be anything
* Also you can describe data object with class-transformer its will be serialize correct automatically


### HttpExceptionFilter
```typescript
@Module({
    providers: [
        {
            provide: APP_FILTER,
            useClass: HttpExceptionFilter
        }        
   ]
})
export class AppModule {}
```
