"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@nestjs/common");
var JsonResponse_1 = require("~/JsonResponse");
/**
 * @package common.exception.filter
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class HttpExceptionFilter
 */
var HttpExceptionFilter = /** @class */ (function () {
    function HttpExceptionFilter() {
    }
    HttpExceptionFilter.prototype.catch = function (exception, host) {
        var ctx = host.switchToHttp();
        var response = ctx.getResponse();
        if (exception instanceof common_1.UnauthorizedException) {
            return response
                .status(401)
                .json(new JsonResponse_1.JsonResponse(undefined, false, "Unauthorized"));
        }
        return response
            .status(200)
            .json(new JsonResponse_1.JsonResponse({
            stackTrace: exception.stack,
            name: exception.name,
        }, false, exception.message));
    };
    HttpExceptionFilter = __decorate([
        common_1.Catch()
    ], HttpExceptionFilter);
    return HttpExceptionFilter;
}());
exports.HttpExceptionFilter = HttpExceptionFilter;
//# sourceMappingURL=HttpExceptionFilter.js.map