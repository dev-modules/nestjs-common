import { ExceptionFilter, ArgumentsHost, Catch, UnauthorizedException } from "@nestjs/common";
import {JsonResponse} from "~/JsonResponse";

/**
 * @package common.exception.filter
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class HttpExceptionFilter
 */
@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: Error, host: ArgumentsHost): any {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        if(exception instanceof UnauthorizedException) {
            return response
                .status(401)
                .json(
                    new JsonResponse(undefined, false, "Unauthorized")
                )
        }

        return response
            .status(200)
            .json(
                new JsonResponse({
                    stackTrace: exception.stack,
                    name: exception.name,
                }, false, exception.message)
            )
    }
}
