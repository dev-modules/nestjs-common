import { Expose } from "class-transformer";

/**
 * @package common
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class SocketResponse
 */
export class SocketResponse<T = any> {

    @Expose() private readonly event: string;
    @Expose() private readonly data: T;

    constructor(event: string, data: T) {
        this.event = event;
        this.data = data;
    }

}
