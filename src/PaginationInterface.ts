/**
 * @package common
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @interface PaginationInterface
 */
export interface PaginationInterface<T = any> {
    perPage: number;
    total: number;
    items: T;
}
