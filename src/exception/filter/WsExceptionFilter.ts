import { Catch, ExceptionFilter, ArgumentsHost } from "@nestjs/common";
import { JsonResponse } from "~/JsonResponse";
import { Socket } from "socket.io";
import { SocketResponse } from "~/SocketResponse";

/**
 * @package common.exception.filter
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class WsExceptionFilter
 */
@Catch()
export class WsExceptionFilter implements ExceptionFilter {
    public async catch(exception: any, host: ArgumentsHost): Promise<void> {
        const context = host.switchToWs();
        const client: Socket = context.getClient();



        client.send(
            new SocketResponse(
                "error",
                {
                    stackTrace: exception.stack,
                    name: exception.name,
                    message: exception.message
                }
            )
        );
    }
}
